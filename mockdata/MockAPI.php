<?php


// Helper method to send a HTTP response code/message
function sendResponse($status = 200, $body = '')
{
    http_response_code($status);
    header('Access-Control-Allow-Origin: *');
    header('Content-type: application/json');
    print $body;
}

$response = array("result"=>"success");
sendResponse(200, json_encode($response));