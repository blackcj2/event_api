<?php
require_once 'BaseAPI.php';
class CheckAuthAPI extends BaseAPI {
    // Main method to redeem a code
    function call() {
        
        if(checkToken()) {
          $this->sendResponse(200, json_encode("result"=>"success"));
        } else {
          $this->sendResponse(401, json_encode("result"=>"failure"));
        }  
    }
}
 
// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new CheckAuthAPI;
$api->call();
?>