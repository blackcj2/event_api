<?php
require_once 'BaseAPI.php';
class GetUsersAPI extends BaseAPI {
    // Main method to redeem a code
    function call() {
        //if ($this->checkToken()) {
          $stmt = $this->db->prepare("SELECT username,id FROM gp_user;");
          $stmt->execute();

          /* bind result variables */
          $stmt->bind_result($user_name, $user_id);
          $rows = array();

          /* fetch values */
          while ($stmt->fetch()) {
              $post = array("name"=>$user_name, 
                            "id"=>$user_id);
              $rows['users'][] = $post;
          }

          $this->sendResponse(200, json_encode($rows));
          $stmt->close();
        //} else {
          //$this->sendResponse(400, json_encode(array("result"=>"failure")));
        //}
    }
}
 
// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new GetUsersAPI;
$api->call();
?>