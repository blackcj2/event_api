<?php
require_once 'BaseAPI.php';
class DeleteUserAPI extends BaseAPI {
    // Main method to redeem a code
    function call() {
        
        if($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["user_id"])) {
            $user_id = $this->db->real_escape_string($_POST['user_id']);
            $sql_insert = "DELETE FROM gp_user WHERE id='$user_id'";
            echo $sql_insert;
            $stmt = $this->db->prepare($sql_insert);
            $stmt->execute();
            $stmt->close();
        }
              
        $this->sendResponse(200, json_encode(array("result"=>"success")));
        
    }
}
 
// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new DeleteUserAPI;
$api->call();
?>
