<?php
require_once 'BaseAPI.php';
class RateEventAPI extends BaseAPI {
    // Main method to redeem a code
    function call() {
        // user_id, even_id, rating
        if(!empty($_POST['user_id'])) {
            $user_id = $this->db->real_escape_string($_POST['user_id']);
          
            $event_id = $this->db->real_escape_string($_POST['event_id']);
            
            $rating = $this->db->real_escape_string($_POST['rating']);

            $sql_insert = "INSERT INTO gp_rating (user_id, event_id, rating) VALUES ('$user_id','$event_id','$rating')";
            
            //echo $sql_insert;
          
            $stmt = $this->db->prepare($sql_insert);
            $stmt->execute();
            $response = array("result"=>"success");
            $this->sendResponse(200, json_encode($response));
            $stmt->close();
        } else {
          $response = array("result"=>"failure");
          $this->sendResponse(400, json_encode($response));
        }
              
        
    }
}
 
// This is the first thing that gets called when this page is loaded
// Creates a new instance of the RedeemAPI class and calls the redeem method
$api = new RateEventAPI;
$api->call();
?>